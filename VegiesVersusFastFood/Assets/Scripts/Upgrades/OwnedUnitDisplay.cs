using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OwnedUnitDisplay : MonoBehaviour
{
    public OwnedUnit ownedUnit;

    public Image unitIcon;
    public Image[] upgradeIcons;
    public Button sellButton;
    public TextMeshProUGUI sellButtonText;

    private UpgradeManager upgradeManager;

    public void Init()
    {
        if (ownedUnit.unit == null)
        {
            sellButton.gameObject.SetActive(false);
            unitIcon.gameObject.SetActive(false);
            foreach (Image upgradeIcon in upgradeIcons)
            {
                upgradeIcon.gameObject.SetActive(false);
            }
        }
        else
        {
            upgradeManager = UpgradeManager.instance;
            List<UpgradeObject> upgrades = upgradeManager.GetUpgrades(ownedUnit.unit);

            sellButton.gameObject.SetActive(true);
            unitIcon.gameObject.SetActive(true);
            unitIcon.sprite = ownedUnit.unit.unitTexture;
            sellButtonText.text = "Sell\n" + ownedUnit.unit.sellValue + " $";
            for (int i = 0; i < upgradeIcons.Length; i++)
            {
                if (upgrades.Count > i)
                {
                    upgradeIcons[i].gameObject.SetActive(true);
                    upgradeIcons[i].sprite = upgrades[i].icon;
                }
                else
                {
                    upgradeIcons[i].gameObject.SetActive(false);
                }
            }
        }
    }
}
