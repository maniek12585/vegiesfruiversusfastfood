using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UnitToBuyDisplay : MonoBehaviour
{
    public UnitToBuy unitToBuy;
    public Image unitIcon;
    public TextMeshProUGUI unitName;
    public TextMeshProUGUI description;
    public TextMeshProUGUI cost;
    public Button buyButton;
    public TextMeshProUGUI buyButtonText;

    public void Init()
    {
        unitIcon.sprite = unitToBuy.unit.unitTexture;
        unitName.text = unitToBuy.unit.unitName;
        description.text = unitToBuy.unit.description;
        cost.text = unitToBuy.unit.cost + " $";
    }
    public void UnitBought()
    {
        buyButton.interactable = false;
        buyButtonText.text = "Bought";
    }

    public void NotEnoughCash()
    {
        buyButton.interactable = false;
        buyButtonText.text = "Can't afford";
    }

    public void EnoughCash()
    {
        buyButton.interactable = true;
        buyButtonText.text = "Buy";
    }

    public void TooManyUnits()
    {
        buyButton.interactable = false;
        buyButtonText.text = "Limit reached";
    }
}
