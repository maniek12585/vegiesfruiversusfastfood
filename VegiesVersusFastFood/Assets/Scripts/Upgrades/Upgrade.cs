using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Upgrade : MonoBehaviour
{
    public UpgradeObject upgradeObject;
    public UnitController unit;
    private UpgradeManager upgradeManager;
    private ShopManager shopManager;
    private UpgradeDisplay upgradeDisplay;

    public void Init()
    {
        shopManager = FindObjectOfType<ShopManager>();
        upgradeDisplay = GetComponent<UpgradeDisplay>();
        upgradeManager = UpgradeManager.instance;
        upgradeDisplay.upgrade = this;
        upgradeDisplay.Init();
    }

    public void CheckCost()
    {
        int cash = GameState.instance.cash;
        if (upgradeManager.GetUpgrades(unit).Count >= upgradeManager.maxUpgrades) upgradeDisplay.TooManyUpgrades();
        else if (cash < upgradeObject.cost) upgradeDisplay.NotEnoughCash();
        else upgradeDisplay.EnoughCash();
    }

    public void Buy()
    {
        shopManager.BuyUpgrade(this);
        upgradeDisplay.UpgradeBought();
    }
}
