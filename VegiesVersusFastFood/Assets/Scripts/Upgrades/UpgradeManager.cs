using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UpgradeManager : MonoBehaviour, IScheduledBehaviour
{
    public int maxUpgrades = 6;
    public static UpgradeManager instance;

    public UnitManager unitManager;
    private Dictionary<string, List<UpgradeObject>> currentUpgrades = new Dictionary<string, List<UpgradeObject>>();
    public List<GameObject> ownedUnits = null;

    public List<UpgradeObject> availableUpgrades;
    public List<UnitController> unitControllers;
    public List<UnitController> playerUnitControllers;

    public int Priority => 2;

    public void OnStartBehaviour()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(gameObject);

        Object[] availableUpgradeAssets = Resources.LoadAll("Upgrades", typeof(UpgradeObject));
        availableUpgrades = new List<UpgradeObject>();
        for (int i = 0; i < availableUpgradeAssets.Length; i++)
        {
            availableUpgrades.Add((UpgradeObject)availableUpgradeAssets[i]);
        }

        Object[] availableUnitsAssets = Resources.LoadAll("Prefabs/Units", typeof(UnitController));
        unitControllers = new List<UnitController>();
        playerUnitControllers = new List<UnitController>();
        for (int i = 0; i < availableUnitsAssets.Length; i++)
        {
            unitControllers.Add((UnitController)availableUnitsAssets[i]);
            if (((UnitController)availableUnitsAssets[i]).isPlayers) playerUnitControllers.Add((UnitController)availableUnitsAssets[i]);
        }

        foreach (UnitController unit in unitControllers)
        {
            currentUpgrades.Add(unit.unitName, new List<UpgradeObject>());
        }
    }

    public void AddUpgrade(UnitController unit, UpgradeObject upgrade)
    {
        currentUpgrades[unit.unitName].Add(upgrade);
    }

    public void AddUnit(GameObject unitSpawner)
    {
        ownedUnits.Add(unitSpawner);
    }

    public void RemoveUnit(int unitSlot)
    {
        ownedUnits.RemoveAt(unitSlot);
    }

    public List<UpgradeObject> GetUpgrades(UnitController unit)
    {
        return currentUpgrades[unit.unitName];
    }
}
