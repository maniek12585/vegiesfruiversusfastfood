using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Upgrade", menuName = "Upgrade")]
public class UpgradeObject : ScriptableObject
{
    public new string name;
    public string description1, description2, description3, description4, description5;
    public Sprite icon;

    public int cost;
    public float rarity;

    public float damageFlat = 0;
    public float damagePercentage = 0;
    public float health = 0;
    public float healthPercentage = 0;
    public float attackRangeFlat = 0;
    public float attackRangePercentage = 0;
    public float attackSpeed = 0;
    public float attackSpeedPercentage = 0;
    public float moveSpeedFlat = 0;
    public float moveSpeedPercentage = 0;
    public float knockbackFlat = 0;
    public float knockbackPercentage = 0;
    public float armorFlat = 0;
    public float armorPercentage = 0;
    public float critChanceFlat = 0;
    public float critChancePercentage = 0;
    public float critMultiFlat = 0;
    public float critMultiPercentage = 0;
    public float dodgeRateFlat = 0;
    public float dodgeRatePercentage = 0;
    public float kitingEfficiencyFlat = 0;
    public float kitingEfficiencyPercentage = 0;
    public float stunChanceFlat = 0;
    public float stunChancePercentage = 0;
}
