using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitToBuy : MonoBehaviour
{
    public UnitController unit;
    private ShopManager shopManager;
    private UnitToBuyDisplay unitToBuyDisplay;
    private UpgradeManager upgradeManager;
    private bool bought = false;

    public void Init()
    {
        upgradeManager = UpgradeManager.instance;
        shopManager = FindObjectOfType<ShopManager>();
        unitToBuyDisplay = GetComponent<UnitToBuyDisplay>();
        unitToBuyDisplay.unitToBuy = this;
        unitToBuyDisplay.Init();
    }

    public void CheckCost()
    {
        if (!bought)
        {
            int cash = GameState.instance.cash;
            if (upgradeManager.ownedUnits.Count >= shopManager.maxUnits) unitToBuyDisplay.TooManyUnits();
            else if (cash < unit.cost) unitToBuyDisplay.NotEnoughCash();
            else unitToBuyDisplay.EnoughCash();
        }
    }

    public void Buy()
    {
        bought = true;
        shopManager.BuyUnit(this);
        unitToBuyDisplay.UnitBought();
    }
}
