using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class OwnedUnit : MonoBehaviour
{
    public UnitController unit;
    private OwnedUnitDisplay ownedUnitDisplay;
    private ShopManager shopManager;
    public int unitSlot;

    public void Init(int unitSlot)
    {
        this.unitSlot = unitSlot;
        shopManager = FindObjectOfType<ShopManager>();
        ownedUnitDisplay = GetComponent<OwnedUnitDisplay>();
        ownedUnitDisplay.ownedUnit = this;
        ownedUnitDisplay.Init();
    }

    public void SellUnit()
    {
        shopManager.SellUnit(unit, unitSlot);
        EventSystem.current.SetSelectedGameObject(null);
    }
}
