using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeDisplay : MonoBehaviour
{
    public Upgrade upgrade;
    public Image unitIcon;
    public Image upgradeIcon;
    public TextMeshProUGUI upgradeName;
    public TextMeshProUGUI description;
    public TextMeshProUGUI cost;
    public Button buyButton;
    public TextMeshProUGUI buyButtonText;

    public void Init()
    {
        unitIcon.sprite = upgrade.unit.unitTexture;
        upgradeIcon.sprite = upgrade.upgradeObject.icon;
        upgradeName.text = upgrade.upgradeObject.name;
        description.text = upgrade.upgradeObject.description1 + "\n" + upgrade.upgradeObject.description2 + "\n" + upgrade.upgradeObject.description3 + "\n" + upgrade.upgradeObject.description4 + "\n" + upgrade.upgradeObject.description5;
        cost.text = upgrade.upgradeObject.cost + " $";
    }
    public void UpgradeBought()
    {
        buyButton.interactable = false;
        buyButtonText.text = "Bought";
    }

    public void NotEnoughCash()
    {
        buyButton.interactable = false;
        buyButtonText.text = "Can't afford";
    }

    public void EnoughCash()
    {
        buyButton.interactable = true;
        buyButtonText.text = "Buy";
    }

    public void TooManyUpgrades()
    {
        buyButton.interactable = false;
        buyButtonText.text = "Limit reached";
    }
}
