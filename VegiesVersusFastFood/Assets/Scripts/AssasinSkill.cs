using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssasinSkill : MonoBehaviour
{
    [SerializeField] private UnitController target;
    [SerializeField] private float skillDelay, initialDelay;
    [SerializeField] private GameObject teleportEffect;
    
    public void SetTarget(UnitController newTarget){
        target = newTarget;
    }

    public IEnumerator Start(){
        yield return new WaitForSeconds(initialDelay);
        StartCoroutine(Teleport());
    }

    private IEnumerator Teleport(){
        if(target != null){
            Instantiate(teleportEffect, transform.position, Quaternion.identity);
            Vector3 directionToTarget = (target.transform.position - transform.position).normalized;
            transform.position = target.transform.position + directionToTarget*1f;
            Instantiate(teleportEffect, transform.position, Quaternion.identity);
        }
        yield return new WaitForSeconds(skillDelay);
        StartCoroutine(Teleport());
    }
}
