﻿using UnityEngine;

public class ExitGameButton : MonoBehaviour, IActionButton
{
    public void CallAction()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_WEBPLAYER
         Application.OpenURL(webplayerQuitURL);
#else
         Application.Quit();
#endif
    }
}
