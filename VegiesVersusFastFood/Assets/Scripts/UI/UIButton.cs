using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface IActionButton
{
    void CallAction();
}

[RequireComponent(typeof(UnityEngine.UI.Button))]
public class UIButton : MonoBehaviour
{
    [SerializeField, RequireInterface(typeof(IActionButton))] UnityEngine.Object _behaviourOnButton;

    UnityEngine.UI.Button button;
    IActionButton behaviourOnObject => _behaviourOnButton as IActionButton;

    private void Awake()
    {
        button = GetComponent<UnityEngine.UI.Button>();
        button.onClick.AddListener(OnUiButtonClick);
    }

    void OnUiButtonClick()
    {
        behaviourOnObject.CallAction();
    }
}
