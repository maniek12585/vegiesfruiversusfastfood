using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartFightButton : MonoBehaviour, IActionButton
{
    [SerializeField] public UnitManager manager;
    void Awake()
    {
        var button = GetComponent<UnityEngine.UI.Button>();
        button.onClick.AddListener(CallAction);
    }

    public void CallAction()
    {
        manager.StartSpawners();
        Destroy(GameObject.Find("PreFight"));
    }
}
