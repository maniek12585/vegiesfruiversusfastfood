﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGameButton : MonoBehaviour, IActionButton
{
    public void CallAction()
    {
        //SceneManager.LoadScene("Shop");
        GameManager.Instance.SetState(StateType.Arena);
    }
}
