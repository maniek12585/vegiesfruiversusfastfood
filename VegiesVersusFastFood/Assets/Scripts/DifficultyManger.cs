﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DifficultyManger : MonoBehaviour
{
    TierType currentTier = TierType.None;

    List<TierType> allTiers;

    [Serializable]
    class SpawnersTier
    {
        public TierType TierType;
        public List<Spawner> tierSpawners;
        public float TierCredits;

        public SpawnersTier(TierType tierType, List<Spawner> tierSpawners, float tierCredits)
        {
            TierType = tierType;
            this.tierSpawners = tierSpawners;
            TierCredits = tierCredits;
        }
    }

    public enum TierType
    {
        None = 0,
        FirstTier = 1,
        SecondTier = 2,
        ThirdTier = 3,
        FourthTier = 4,
        FifthTier = 5,
        SixthTier = 6,
        SeventhTier = 7,
        EightthTier = 8,
        NinethTier = 9,
        TenthTier = 10,
        EleventhTier = 11,
        TwelvthTier = 12,
        ThitinthTier = 13,
        Fourteenth = 14,
        Fifteenth = 15,
        Sixteennth = 16,
        Seventeenth = 17,
        Eigthteenth = 18,
        Nineteenth = 19,
        Twentieth = 20,
    }

    [HideInInspector] public List<Spawner> newEnemySpawners = new List<Spawner>();

    [SerializeField] List<SpawnersTier> spawnersPerTier = new List<SpawnersTier>();
    [Header("spawners put in folders")]
    [SerializeField] List<Spawner> enemySpawnerTier1;
    [SerializeField] List<Spawner> enemySpawnerTier2;
    [SerializeField] List<Spawner> enemySpawnerTier3;


    SpawnersTier maxTier;
    public static DifficultyManger Instance;
    const float spawnDelayFactor = .5f;
    const int spawnLimitFactor = 1;
    const int baseCreditForTier = 25;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);

        allTiers = Enum.GetValues(typeof(TierType)).Cast<TierType>().ToList();
        //allTiers.Remove(TierType.None);

        LoadAllAvailableTiers();

        maxTier = spawnersPerTier.Last();
    }

    [ContextMenu("Asda")]
    public void LoadAllAvailableTiers()
    {
        UnityEngine.Random.InitState((int)System.DateTime.Now.Ticks);
        for (int i = 0; i < allTiers.Count; i++)
        {
            SpawnersTier spawnersTier = new SpawnersTier
            (
                allTiers[i],
                new List<Spawner>()
                {
                    enemySpawnerTier1[UnityEngine.Random.Range(0, enemySpawnerTier1.Count)],
                    enemySpawnerTier2[UnityEngine.Random.Range(0, enemySpawnerTier2.Count)],
                    enemySpawnerTier3[UnityEngine.Random.Range(0, enemySpawnerTier3.Count)]
                },
                baseCreditForTier + i * 5
            );
            spawnersPerTier.Add(spawnersTier);
        }
    }

    public void MakeGameHarder(int arenasWon)
    {
        if (arenasWon % 2 == 0)
        {
            if (currentTier != allTiers.Last())
            {
                var previousTier = currentTier;
                UpgradeTier();
                if (currentTier == previousTier) return;
                newEnemySpawners = GetSpawnersForNewTier();
            }
            else
            {
                foreach (var spawner in newEnemySpawners)
                {
                    float currentSpawnDelay = spawner.SpawnDelay_;
                    int currentSpawnLimit = spawner.SpawnLimit;

                    spawner.ChangeSpawnDelay(currentSpawnDelay - spawnDelayFactor >= 0 ? currentSpawnDelay - spawnDelayFactor : 0);
                    spawner.ChangeSpawnLimit(currentSpawnLimit + spawnLimitFactor);
                }
            }
        }else{
            newEnemySpawners = GetSpawnersForNewTier();
        }
    }

    private List<Spawner> GetSpawnersForNewTier()
    {
        SpawnersTier newTier = null;
        foreach (var data in spawnersPerTier)
        {
            if (data.TierType == currentTier)
            {
                newTier = data;
                break;
            }
        }
        if (newTier != null)
        {
            List<Spawner> list = new List<Spawner>();
            newEnemySpawners.Clear();
            var tierCost = newTier.TierCredits;
            while (tierCost > 0)
            {
                var randomTierSpawner = newTier.tierSpawners[UnityEngine.Random.Range(0, newTier.tierSpawners.Count)];
                tierCost -= randomTierSpawner.SpawnCost;
                list.Add(randomTierSpawner);
            }
            return list;
        }
        return newEnemySpawners;
    }

    private void UpgradeTier()
    {
        int idx = allTiers.IndexOf(currentTier);
        if (idx == -1) return;
        if (idx + 1 >= allTiers.Count) return;
        currentTier = allTiers[idx + 1];
    }
}