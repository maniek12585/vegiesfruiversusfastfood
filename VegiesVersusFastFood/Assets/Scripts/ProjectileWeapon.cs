using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileWeapon : WeaponInterface
{
    private Rigidbody2D rb2d;

    [SerializeField] private GameObject Projectile;
    [SerializeField] private GameObject spriteContainerOuter, spriteContainerInner;

    private Animator animator;
    // Start is called before the first frame update
    void Awake()
    {
        rb2d = GetComponentInChildren<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        if(target != null){
            Vector2 vectorToTarget = target.transform.position - transform.parent.position;

            //rotate weapon towards enemy
            var angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg + correctAngle;
            spriteContainerOuter.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
        if(canAttack && target != null){
            canAttack = false;
            GameObject projectile = Instantiate(Projectile, transform.position, transform.rotation);
            projectile.GetComponent<Projectile>().SetTargetAndStuff(target, damage, critChance, critMulti, knockback, IsStun());
            animator.SetTrigger("Shoot");
        }
    }

    new public void AttackWithWeapon(){
        canAttack = true;
    }
}
