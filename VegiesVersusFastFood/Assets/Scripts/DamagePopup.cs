using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DamagePopup : MonoBehaviour
{
    private Rigidbody2D rb2d;
    [SerializeField] private TextMeshPro textMesh;

    private void Awake() {
        textMesh = GetComponent<TextMeshPro>();
        GameObject.Destroy(gameObject, 0.5f);
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.AddForce(new Vector3(Random.Range(-50f,50f), 150f, 0f));
    }

    public void SetValue(float damage, bool isCrit, bool isHeal){
        if(isHeal){
            float roundedHeal = Mathf.RoundToInt(damage * 10f) / 10f;
            if(isCrit){
                textMesh.text = "+" + roundedHeal.ToString() + "!";
            }
            else textMesh.text = "+" + roundedHeal.ToString();
        }else{
            if(damage <= Mathf.Epsilon){
                textMesh.text = "miss";
                textMesh.fontSize *= 0.9f;
            }else{
                float roundedDamage = Mathf.RoundToInt(damage * 10f) / 10f;
                if(isCrit){
                    textMesh.text = roundedDamage.ToString() + "!";
                }
                else textMesh.text = roundedDamage.ToString();
            }
        }
    }
}
