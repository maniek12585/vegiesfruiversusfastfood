﻿using UnityEngine;

public class ArenaState : MonoBehaviour, IState
{
    public StateType StateType => StateType.Arena;

    public void EnterState()
    {
        LoadingManager.Instance.LoadScene(SceneName.Arena);
        //Show ui for arenas 
    }

    public void ExitState()
    {
        // hide ui if necessary 
    }
}

