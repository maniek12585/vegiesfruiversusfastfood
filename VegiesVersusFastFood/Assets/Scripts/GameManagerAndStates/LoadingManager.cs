﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using static ExtensionMethods;

public enum SceneName
{ 
    None = 0,
    Init = 1,
    MainMenu = 2,
    Arena = 3,
    AfterBattle = 4,
}

public class LoadingManager : Singleton<LoadingManager>
{
    SceneName loadedScene = SceneName.None;
    public Action<SceneName> OnSceneLoaded;
    public Action<SceneName> OnSceneUnLoaded;

    protected override void OnAwakeSingleton()
    {
        Persistent = true;
        base.OnAwakeSingleton();
    }

    public void LoadScene(SceneName scene, bool unloadOldScenes = true)
    {
        if (scene != SceneName.None && !SceneManager.GetSceneByName(scene.ToString()).isLoaded)
        {
            var asyncOperation = SceneManager.LoadSceneAsync(scene.ToString(), LoadSceneMode.Single);
        }
        loadedScene = scene;
    }
}