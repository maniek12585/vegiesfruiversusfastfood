using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    [SerializeField,RequireInterface(typeof(IState))] List<UnityEngine.Object> _gameStates;

    IState state;

    List<IState> gameStates => _gameStates.ConvertAll(x => x as IState);

    protected override void OnAwakeSingleton()
    {
        base.OnAwakeSingleton();
        Persistent = true;
    }

    protected override void OnStartSingleton()
    {
        base.OnStartSingleton();
        SetState(StateType.MainMenu);
    }

    public void SetState(StateType stateType)
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}

interface IState
{
    public StateType StateType { get; }
   
    public void EnterState();
    public void HandleInput() { }
    public void ExitState();
    public void UpdateState() { }
}

[System.Flags]
public enum StatesUI
{
    None = 0,
    MainMenuUI = 1<<0,

    //arena ui types

    //after battle ui
}

public enum StateType
{
    None = 0,
    MainMenu = 1,
    Arena = 2,
    AfterBattle = 3,
}