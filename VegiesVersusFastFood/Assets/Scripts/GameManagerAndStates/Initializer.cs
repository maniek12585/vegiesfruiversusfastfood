using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interface used for scheduled behaviour managing
/// </summary>
public interface IScheduledBehaviour
{
    /// <summary>
    /// Priority of this scheduler. It indicates priority in which scheduler will be called. Must be implemented
    /// </summary>
    public int Priority { get; }
    void OnAwakeBehaviour() { }
    void OnStartBehaviour() { }
    void OnUpdateBehaviour() { }
    void OnFixedUpdateBehaviour() { }
    void OnDestroybehaviour() { }
}

public class Initializer : MonoBehaviour
{
    SortedSet<IScheduledBehaviour> behaviours;

    class BehavioursComparer : Comparer<IScheduledBehaviour>
    {
        public override int Compare(IScheduledBehaviour x, IScheduledBehaviour y)
        {
            return Comparer<double>.Default.Compare(x.Priority, y.Priority);
        }
    }

    private void Awake()
    {
        var notSortedBehaviours = ExtensionMethods.FindInterfacesOfType<IScheduledBehaviour>();

        behaviours = new SortedSet<IScheduledBehaviour>(new BehavioursComparer());

        notSortedBehaviours.ForEach(x => behaviours.Add(x));

        foreach (var setElement in behaviours)
        {
            setElement.OnAwakeBehaviour();
        }
    }

    private void Start()
    {
        foreach (var setElement in behaviours)
        {
            setElement.OnStartBehaviour();
        }
    }

    private void Update()
    {
        foreach (var setElement in behaviours)
        {
            setElement.OnUpdateBehaviour();
        }
    }

    private void FixedUpdate()
    {
        foreach (var setElement in behaviours)
        {
            setElement.OnFixedUpdateBehaviour();
        }
    }

    private void OnDestroy()
    {
        foreach (var setElement in behaviours)
        {
            setElement.OnDestroybehaviour();
        }
    }
}
