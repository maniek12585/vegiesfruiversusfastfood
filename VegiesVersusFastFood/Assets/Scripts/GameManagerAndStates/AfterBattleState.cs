﻿using UnityEngine;

public class AfterBattleState : MonoBehaviour, IState
{
    public StateType StateType => StateType.AfterBattle;

    public void EnterState()
    {
        //load afterbattle scene with shop ui and ui with choosing prices after battle
    }

    public void ExitState()
    {
        // hide ui for states
    }
}

