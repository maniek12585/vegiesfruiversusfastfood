﻿using UnityEngine;

public class MainMenuState : MonoBehaviour, IState
{
    public StateType StateType => StateType.MainMenu;

    public void EnterState()
    {
        LoadingManager.Instance.LoadScene(SceneName.MainMenu);
        //ShowMainMenuUI
    }

    public void ExitState()
    {
        //HideMainMenuUI
    }
}

