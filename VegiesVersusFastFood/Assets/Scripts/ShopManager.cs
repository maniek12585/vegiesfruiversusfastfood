using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour
{
    public GameObject upgradesUI;
    public GameObject unitsToBuyUI;
    public GameObject ownedUnitsUI;
    public TextMeshProUGUI cashDisplay;
    public Button continueButton;

    private UpgradeManager upgradeManager;
    private GameState gameState;

    private List<Upgrade> currentUpgrades;
    private List<UnitToBuy> currentUnitsToBuy;

    public int maxUnits = 0;
    private bool continueDisabled = false;

    // Start is called before the first frame update
    void Start()
    {
        currentUpgrades = new List<Upgrade>();
        currentUnitsToBuy = new List<UnitToBuy>();
        upgradeManager = UpgradeManager.instance;
        gameState = GameState.instance;

        // update cash display to current value
        cashDisplay.text = gameState.cash + " $";

        List<int> upgradesLeft = new List<int>();
        for (int i = 0; i < upgradeManager.availableUpgrades.Count; i++) upgradesLeft.Add(i);

        foreach (Transform unitOwned in ownedUnitsUI.transform)
        {
            maxUnits++;
        }

        foreach (Transform upgradeTransform in upgradesUI.transform)
        {
            // assign random unit and upgrade to upgrade prefab. Upgrades can't repeat, units can
            int randomUpgradeListIndex = Random.Range(0, upgradesLeft.Count);
            int randomUpgradeIndex = upgradesLeft[randomUpgradeListIndex];
            upgradesLeft.RemoveAt(randomUpgradeListIndex);
            UpgradeObject upgradeObject = upgradeManager.availableUpgrades[randomUpgradeIndex];
            Upgrade upgrade = upgradeTransform.GetComponent<Upgrade>();
            currentUpgrades.Add(upgrade);
            upgrade.upgradeObject = upgradeObject;
            int randomUnitIndex = Random.Range(0, upgradeManager.playerUnitControllers.Count);
            UnitController unit = upgradeManager.playerUnitControllers[randomUnitIndex];
            upgrade.unit = unit;
            upgrade.Init();
            upgrade.CheckCost();
        }

        foreach (Transform unitToBuyTransform in unitsToBuyUI.transform)
        {
            // assign random unit to unitToBuy prefab. Units can repeat
            int randomUnitIndex = Random.Range(0, upgradeManager.playerUnitControllers.Count);
            UnitController unit = upgradeManager.playerUnitControllers[randomUnitIndex];
            UnitToBuy unitToBuy = unitToBuyTransform.GetComponent<UnitToBuy>();
            currentUnitsToBuy.Add(unitToBuy);
            unitToBuy.unit = unit;
            unitToBuy.Init();
            unitToBuy.CheckCost(); 
        }

        UpdateOwnedUnits();
    }

    public void BuyUpgrade(Upgrade upgrade)
    {
        SetCash(gameState.cash - upgrade.upgradeObject.cost);
        upgradeManager.AddUpgrade(upgrade.unit, upgrade.upgradeObject);
        UpdateDisplay();
    }

    public void BuyUnit(UnitToBuy unitToBuy)
    {
        SetCash(gameState.cash - unitToBuy.unit.cost);
        upgradeManager.AddUnit(unitToBuy.unit.spawnerPrefab);
        UpdateDisplay();
        if (continueDisabled)
        {
            continueDisabled = false;
            continueButton.interactable = true;
        }
    }

    public void SellUnit(UnitController unit, int unitSlot)
    {
        SetCash(gameState.cash + unit.sellValue);
        upgradeManager.RemoveUnit(unitSlot);
        UpdateDisplay();
        if (upgradeManager.ownedUnits.Count == 0)
        {
            continueDisabled = true;
            continueButton.interactable = false;
        }
    }

    public void UpdateDisplay()
    {
        CheckCosts();
        UpdateOwnedUnits();
    }

    public void CheckCosts()
    {
        foreach (Upgrade currentUnpgrade in currentUpgrades)
        {
            currentUnpgrade.CheckCost();
        }
        foreach (UnitToBuy unitToBuy in currentUnitsToBuy)
        {
            unitToBuy.CheckCost();
        }
    }

    public void UpdateOwnedUnits()
    {
        int currentUnitSlot = 0;
        foreach (Transform unitOwned in ownedUnitsUI.transform)
        {
            OwnedUnit ownedUnit = unitOwned.GetComponent<OwnedUnit>();

            if (upgradeManager.ownedUnits.Count > currentUnitSlot)
            {
                ownedUnit.unit = upgradeManager.ownedUnits[currentUnitSlot].GetComponent<Spawner>().unitToSpawn.GetComponent<UnitController>();
            }
            else ownedUnit.unit = null;
            ownedUnit.Init(currentUnitSlot);

            currentUnitSlot++;
        }
    }

    public void SetCash(int newValue)
    {
        gameState.cash = newValue;
        cashDisplay.text = "Cash: " + newValue + " $";
    }

    public void Continue()
    {
        gameState.state = State.InArena;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }
}
