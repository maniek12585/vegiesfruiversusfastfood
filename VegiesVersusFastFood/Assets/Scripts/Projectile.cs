using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private UnitController target;
    [SerializeField] private float damage, speed =1f, critChance, critMulti, knockback;
    private Rigidbody2D rb2d;
    private Vector2 vectorToTarget;
    [SerializeField] private bool isLobbed = false, isGrenade = false, friendly = true, stun = false, isHealing = false;
    private int iteration = 0;
    private bool isCrit = false;

    [SerializeField] private GameObject EXPLOSION;

    // stuff for lobbing
    private Vector3 startPoint, staticTarget, lobAssistPoint, assistPointLeft, assistPointRight;

    private float currdegrees = 0f;

    public void SetTargetAndStuff(UnitController newTarget, float newDamage, float newCritChance, float newCritMulti, float newKnockback, bool isStun){
        stun = isStun;
        target = newTarget;
        damage = newDamage;
        critChance = newCritChance;
        critMulti = newCritMulti;
        knockback = newKnockback;
        if(isLobbed){
            startPoint = transform.position;
            if(isGrenade){
                staticTarget = new Vector2(target.transform.position.x + Random.Range(-0.5f, 0.5f), target.transform.position.y+ Random.Range(-0.5f, 0.5f));
                lobAssistPoint = new Vector2((startPoint.x + staticTarget.x)/2, 5f + (startPoint.y + staticTarget.y)/2);
            }else{
                staticTarget = target.transform.position;
                lobAssistPoint = new Vector2((startPoint.x + staticTarget.x)/2, 5f + (startPoint.y + staticTarget.y)/2);
            }
            assistPointLeft = startPoint;
            assistPointRight = lobAssistPoint;
            iteration = 0;
        }
        isCrit = (Random.Range(0, 1f) < critChance);
    }
    
    private void Awake() {
        rb2d = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate() {
        if(staticTarget != null && isGrenade)
        {
            GrenadeBehaviour();
        }
        else if(target != null){
            vectorToTarget = target.transform.position - transform.position;
            float distanceToTarget = 100f;
            if(!isLobbed){
                RotateTowardsTarget();
                Vector2 newPosition = Vector2.MoveTowards(transform.position, target.transform.position, Time.fixedDeltaTime * 15 * speed);
                rb2d.MovePosition(newPosition);
                distanceToTarget = (transform.position - target.transform.position).magnitude;
            }else{
                iteration++;
                RotateAround();
                assistPointLeft = Vector3.Lerp(startPoint, lobAssistPoint, Time.fixedDeltaTime * speed * iteration);
                assistPointRight = Vector3.Lerp(lobAssistPoint, target.transform.position, Time.fixedDeltaTime * speed * iteration);
                Vector2 newPosition = Vector3.Lerp(assistPointLeft, assistPointRight, Time.fixedDeltaTime * speed * iteration);
                rb2d.MovePosition(newPosition);
                distanceToTarget = (transform.position - target.transform.position).magnitude;
            }
            if (distanceToTarget < 0.5f)
            {
                // damage the unit
                DamageTarget(target);
                Destroy(gameObject);
            }
        }
        else{
            StartCoroutine(DestroyIfNoTarget());
        }
    }

    private void GrenadeBehaviour()
    {
        iteration++;
        RotateAround();
        assistPointLeft = Vector2.Lerp(startPoint, lobAssistPoint, Time.fixedDeltaTime * speed * iteration);
        assistPointRight = Vector2.Lerp(lobAssistPoint, staticTarget, Time.fixedDeltaTime * speed * iteration);
        Vector2 newPosition = Vector2.Lerp(assistPointLeft, assistPointRight, Time.fixedDeltaTime * speed * iteration);
        rb2d.MovePosition(newPosition);
        float distanceToTarget = (transform.position - staticTarget).magnitude;
        if (distanceToTarget < 0.1f)
        {
            // damage all units in an area unit
            GameObject explosion = Instantiate(EXPLOSION, transform.position, Quaternion.identity);
            Destroy(explosion, 1f);
            Collider2D[] targetsInExplosion = Physics2D.OverlapCircleAll(transform.position, 1.5f);
            foreach (Collider2D unit in targetsInExplosion)
            {
                UnitController unitController = unit.GetComponent<UnitController>();
                if (unitController != null)
                {
                    if (unitController.IsPlayers() != friendly)
                    {
                        DamageTarget(unitController);
                    }
                }
            }
            Destroy(gameObject);
        }
    }

    private void DamageTarget(UnitController target)
    {
        if(isHealing){
            if(isCrit){
                target.GetHealed(damage * critMulti, true);
            }else{
                target.GetHealed(damage, false);
            }
        }else{
            if (target.isDodge())
            {
                target.GetDamaged(0f, false);
            }
            else{
                if(stun){
                    target.GetStunned(1.2f);
                } if (isCrit)
                {
                    target.GetDamaged(damage * critMulti, true);
                    applyKnockback(2f, target);
                }
                else
                {
                    target.GetDamaged(damage, false);
                    applyKnockback(1f, target);
                }
            }
        }
    }

    private void RotateTowardsTarget(){
        currdegrees = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg; //+ correctAngle;
        transform.rotation = Quaternion.AngleAxis(currdegrees, Vector3.forward);
    }

    private void RotateAround(){
        currdegrees = currdegrees - Time.fixedDeltaTime * 500;
        transform.rotation = Quaternion.AngleAxis(currdegrees, Vector3.forward);
    }

    private void applyKnockback(float multi, UnitController target){
        Rigidbody2D targetBody = target.GetComponent<Rigidbody2D>();
        vectorToTarget = target.transform.position - transform.position;
        if (targetBody != null)
        {
            targetBody.AddForce(vectorToTarget.normalized * 100 * knockback * multi);
        }
    }

    private IEnumerator DestroyIfNoTarget(){
        yield return new WaitForSeconds(0.1f);
        if(target == null) Destroy(gameObject);
    }
}
