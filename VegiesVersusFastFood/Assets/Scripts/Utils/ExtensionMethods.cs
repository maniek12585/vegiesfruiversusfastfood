﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class ExtensionMethods
{
	public static List<T> FindInterfacesOfType<T>()
	{
		List<T> interfaces = new();
		GameObject[] rootGameObjects = SceneManager.GetActiveScene().GetRootGameObjects();

		foreach (var rootGameObject in rootGameObjects)
		{
			T[] childrenInterfaces = rootGameObject.GetComponentsInChildren<T>();
			foreach (var childInterface in childrenInterfaces)
			{
				interfaces.Add(childInterface);
			}
		}

		return interfaces;
	}

	public static IEnumerable<Enum> GetFlags(Enum input)
	{
		foreach (Enum value in Enum.GetValues(input.GetType()))
			if (input.HasFlag(value))
				yield return value;
	}
}
