using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseCollider : MonoBehaviour
{
    public GameObject TriggeredTile;
    [SerializeField] private Rigidbody2D rbody;
    private Vector3 mousePos, newPos;
    private BoxCollider2D collider;
    void Start()
    {
        collider = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        mousePos = Input.mousePosition;
        newPos = Camera.main.ScreenToWorldPoint(mousePos);
        rbody.MovePosition(newPos);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision.gameObject.name);
        TriggeredTile = collision.gameObject;
    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.gameObject.name.Contains("Spawner"))
    //    {
    //        Physics2D.IgnoreCollision(collision.collider, collider);
    //    }
    //}
}
