using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitDisplay : MonoBehaviour
{
    [SerializeField] private SpriteRenderer mainSpriteRenderer, weaponSpriteRenderer;
    [SerializeField] private GameObject damageNumber, critDamageNumber, healNumber, critHealNumber;

    public void Init()
    {
    }

    private void Update() {
        mainSpriteRenderer.sortingOrder = 1000 - Mathf.RoundToInt(transform.position.y * 10f);
        weaponSpriteRenderer.sortingOrder = 1001 - Mathf.RoundToInt(transform.position.y * 10f);
    }

    public void SpawnDamageNumber(float number, bool isCrit){
        if(isCrit){
            GameObject damageNum = GameObject.Instantiate(critDamageNumber, transform.position, Quaternion.identity);
            damageNum.GetComponent<DamagePopup>().SetValue(number, isCrit, false);
        }
        else{
            GameObject damageNum = GameObject.Instantiate(damageNumber, transform.position, Quaternion.identity);
            damageNum.GetComponent<DamagePopup>().SetValue(number, isCrit, false);
        }
        //damageNum.transform.SetParent(transform);
    }

    public void SpawnHealNumber(float number, bool isCrit){
        if(isCrit){
            GameObject damageNum = GameObject.Instantiate(critHealNumber, transform.position, Quaternion.identity);
            damageNum.GetComponent<DamagePopup>().SetValue(number, isCrit, true);
        }
        else{
            GameObject damageNum = GameObject.Instantiate(healNumber, transform.position, Quaternion.identity);
            damageNum.GetComponent<DamagePopup>().SetValue(number, isCrit, true);
        }
        //damageNum.transform.SetParent(transform);
    }
}
