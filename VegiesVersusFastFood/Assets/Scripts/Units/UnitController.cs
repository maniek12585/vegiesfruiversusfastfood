using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitController : MonoBehaviour
{
    public string unitName;
    public string description;
    public Sprite unitTexture;
    public GameObject spawnerPrefab;
    public int cost;
    public int sellValue;
    [SerializeField] private float damage, health, moveSpeed, attackRange, attackSpeed, knockback, armor = 0, stunChance = 0; //, stunDuration;
    [SerializeField] private float distanceToTarget = 100f, kitingEfficiency = 0.5f, critChance = 0.05f, critMulti = 2f, dodgeRate = 0.05f;
    private float currentHealth;
    public bool isPlayers, canKite, isHealer, isPuller;
    private bool isEnemyInRange, canPull = false, pulled = false;
    private bool facingLeft = false, died = false;
    private float nextAttackTime = 0f, lastAttackTime = -1f, baseAttackRange, baseKnockback;
    private float damageReceived;

    //private List<UnitController> OpposingUnits, FriendlyUnits;
    private UnitManager unitManager;

    private UnitController[] OtherUnits;

    private UnitController targetUnit, farUnit, weakUnit, weakAllyUnit; // closest, farthest, lowestHP enemy unit

    private Vector2 moveVector, moveDirection;

    private Rigidbody2D rb2d;

    private WeaponInterface weapon;

    private AssasinSkill assasinSkill;
    private bool isAssasin = false;

    [SerializeField] private SpriteRenderer HealthBar;
    [SerializeField] private GameObject StunIndicator;

    private UnitDisplay unitDisplay;

    private GameState gameState;

    public void Init()
    {
        unitManager = FindObjectOfType<UnitManager>();
        UpgradeManager upgradeManager = FindObjectOfType<UpgradeManager>();
        List<UpgradeObject> upgrades = upgradeManager.GetUpgrades(this);

        float damageFlatTotal = 0;
        float damagePercentageTotal = 0;
        float healthFlatTotal = 0;
        float healthPercentageTotal = 0;
        float attackRangeFlatTotal = 0;
        float attackRangePercentageTotal = 0;
        float attackSpeedFlatTotal = 0;
        float attackSpeedPercentageTotal = 0;
        float moveSpeedFlatTotal = 0;
        float moveSpeedPercentageTotal = 0;
        float knockbackFlatTotal = 0;
        float knockbackPercentageTotal = 0;
        float armorFlatTotal = 0;
        float armorPercentageTotal = 0;
        float critChanceFlatTotal = 0;
        float critChancePercentageTotal = 0;
        float critMultiFlatTotal = 0;
        float critMultiPercentageTotal = 0;
        float dodgeRateFlatTotal = 0;
        float dodgeRatePercentageTotal = 0;
        float kitingEfficiencyFlatTotal = 0;
        float kitingEfficiencyPercentageTotal = 0;
        float stunChanceFlatTotal = 0;
        float stunChancePercentageTotal = 0;

        foreach (UpgradeObject upgrade in upgrades)
        {
            damageFlatTotal += upgrade.damageFlat;
            damagePercentageTotal += upgrade.damagePercentage;
            healthFlatTotal += upgrade.health;
            healthPercentageTotal += upgrade.healthPercentage;
            attackRangeFlatTotal += upgrade.attackRangeFlat;
            attackRangePercentageTotal += upgrade.attackRangePercentage;
            attackSpeedFlatTotal += upgrade.attackSpeed;
            attackSpeedPercentageTotal += upgrade.attackSpeedPercentage;
            moveSpeedFlatTotal += upgrade.moveSpeedFlat;
            moveSpeedPercentageTotal += upgrade.moveSpeedPercentage;
            knockbackFlatTotal += upgrade.knockbackFlat;
            knockbackPercentageTotal += upgrade.knockbackPercentage;
            armorFlatTotal += upgrade.armorFlat;
            armorPercentageTotal += upgrade.armorPercentage;
            critChanceFlatTotal += upgrade.critChanceFlat;
            critChancePercentageTotal += upgrade.critChancePercentage;
            critMultiFlatTotal += upgrade.critMultiFlat;
            critMultiPercentageTotal += upgrade.critMultiPercentage;
            dodgeRateFlatTotal += upgrade.dodgeRateFlat;
            dodgeRatePercentageTotal += upgrade.dodgeRatePercentage;
            kitingEfficiencyFlatTotal += upgrade.kitingEfficiencyFlat;
            kitingEfficiencyPercentageTotal += upgrade.kitingEfficiencyPercentage;
            stunChanceFlatTotal += upgrade.stunChanceFlat;
            stunChancePercentageTotal += upgrade.stunChancePercentage;
        }

	    damage = (damage + damageFlatTotal) * (1f + damagePercentageTotal);
	    health = (health + healthFlatTotal) * (1f + healthPercentageTotal);
	    moveSpeed = (moveSpeed + moveSpeedFlatTotal) * (1f + moveSpeedPercentageTotal);
	    attackRange = (attackRange + attackRangeFlatTotal) * (1f + attackRangePercentageTotal);
	    attackSpeed = (attackSpeed + attackSpeedFlatTotal) * (1f + attackSpeedPercentageTotal);
        knockback = (knockback + knockbackFlatTotal) * (1f + knockbackPercentageTotal);
        armor = (armor + armorFlatTotal) * (1f + armorPercentageTotal);
        critChance = (critChance + critChanceFlatTotal) * (1f + critChancePercentageTotal);
        critMulti = (critMulti + critMultiFlatTotal) * (1f + critMultiPercentageTotal);
        dodgeRate = (dodgeRate + dodgeRateFlatTotal) * (1f + dodgeRatePercentageTotal);
        kitingEfficiency = (kitingEfficiency + kitingEfficiencyFlatTotal) * (1f + kitingEfficiencyPercentageTotal);
        stunChance = (stunChance + stunChanceFlatTotal) * (1f + stunChancePercentageTotal);

        // round all values to 2 decimal places
        damage = Mathf.Round(damage * 100f) / 100f;
        health = Mathf.Round(health * 100f) / 100f;
        moveSpeed = Mathf.Round(moveSpeed * 100f) / 100f;
        attackRange = Mathf.Round(attackRange * 100f) / 100f;
        attackSpeed = Mathf.Round(attackSpeed * 100f) / 100f;
        knockback = Mathf.Round(knockback * 100f) / 100f;
        armor = Mathf.Round(armor * 100f) / 100f;
        critChance = Mathf.Round(critChance * 100f) / 100f;
        critMulti = Mathf.Round(critMulti * 100f) / 100f;
        dodgeRate = Mathf.Round(dodgeRate * 100f) / 100f;
        kitingEfficiency = Mathf.Round(kitingEfficiency * 100f) / 100f;

        StartCoroutine(ScanForEnemyTarget());
        rb2d = GetComponent<Rigidbody2D>();
        weapon = GetComponentInChildren<WeaponInterface>();
        assasinSkill = GetComponent<AssasinSkill>();
        if(assasinSkill != null) isAssasin = true;
        weapon.setDamageAndStuff(damage, critChance, critMulti, knockback, stunChance);
        currentHealth = health;
        unitDisplay = GetComponent<UnitDisplay>();
        damageReceived = 100/(100+armor);
        gameState = GameState.instance;
        if(isPuller){
            baseAttackRange = attackRange;
            baseKnockback = knockback;
            StartCoroutine(PullerSkill());
        }

    }

    private IEnumerator PullerSkill(){
        yield return new WaitForSeconds(5f);
        attackRange = baseAttackRange * 4f;
        canPull = true;
        //pulled = false;
    }
    
    public bool isDodge(){
        return (Random.Range(0,1f) < dodgeRate);
    }

    public bool IsPlayers(){
        return isPlayers;
    }
    
    public float GetCurrHealth(){
        return currentHealth;
    }
    public float GetCurrHealthPercentage(){
        return currentHealth/health;
    }

    void Update()
    {
        if(targetUnit != null){
            moveVector = targetUnit.transform.position - transform.position;
            distanceToTarget = moveVector.magnitude;
            moveDirection = moveVector.normalized;
            if(moveDirection.x < 0 && !facingLeft){
                facingLeft = true;
                transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x)*-1f, transform.localScale.y, 1);
                weapon.FlippedLeft(true);
            }else if(moveDirection.x > 0 && facingLeft){
                facingLeft = false;
                transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, 1);
                weapon.FlippedLeft(false);
            }
            if(isEnemyInRange && nextAttackTime < Time.timeSinceLevelLoad){
                //if(pulled) canPull = false;
                if(canPull){
                    knockback = baseKnockback * -4;
                }
                nextAttackTime = Time.timeSinceLevelLoad + 1/attackSpeed;
                if(isPuller) weapon.setDamageAndStuff(damage, critChance, critMulti, knockback, stunChance);
                weapon.AttackWithWeapon();
                if(isPuller && canPull){
                    StartCoroutine(PullerSkill());
                    attackRange = baseAttackRange;
                    knockback = baseKnockback;
                    //pulled = true;
                    StartCoroutine(NoPull());
                }
                StartCoroutine(PauseKiting());
            }
        }else{
            distanceToTarget = 100f;
        }
    }

    private IEnumerator NoPull(){
        yield return new WaitForSeconds(1f);
        canPull = false;
    }

    private void FixedUpdate() {
        if(targetUnit != null){
            // move towards target if out of range
            float diff = distanceToTarget - attackRange;
            if(diff > 0){
                rb2d.AddForce(moveDirection * Time.fixedDeltaTime * moveSpeed * 10 * (rb2d.mass/6));
                isEnemyInRange = false;
            }else{
                isEnemyInRange = true;
            }
            if(diff < -0.3f && canKite){
                rb2d.AddForce(moveDirection * Time.fixedDeltaTime * moveSpeed * -10 * kitingEfficiency * (rb2d.mass/6));
            }
        }
    }

    private IEnumerator ScanForEnemyTarget(){
        yield return new WaitForSeconds(0.5f);

        // find all units

        OtherUnits = FindObjectsOfType<UnitController>();

        float closeDistance = 10000f, farDistance = 0f, minHealth = 1000f, minAllyHealth = 1000f;
        foreach( UnitController unit1 in OtherUnits){
            if((unit1.isPlayers && !isPlayers) || (!unit1.isPlayers && isPlayers)){
                float distanceToUnit = (transform.position - unit1.transform.position).magnitude;
                float hp = unit1.GetCurrHealth();
                if(distanceToUnit < closeDistance){
                    closeDistance = distanceToUnit;
                    targetUnit = unit1;
                }if(distanceToUnit > farDistance){
                    farDistance = distanceToUnit;
                    farUnit = unit1;
                }if(hp < minHealth){
                    minHealth = hp;
                    weakUnit = unit1;
                }
            }else if(isHealer){
                if(((unit1.isPlayers && isPlayers) || (!unit1.isPlayers && !isPlayers)) && unit1 != this){
                    float hpPercent = unit1.GetCurrHealthPercentage();
                    if(hpPercent < minAllyHealth){
                        minAllyHealth = hpPercent;
                        weakAllyUnit = unit1;
                    }
                }
            }
        }
        if(isHealer) targetUnit = weakAllyUnit;
        if(isPuller && canPull){
            targetUnit = farUnit;
            weapon.SetWeaponTarget(targetUnit);
            //yield return new WaitForSeconds(0.7f);
        } else weapon.SetWeaponTarget(targetUnit);
        if(isAssasin) assasinSkill.SetTarget(weakUnit);
        StartCoroutine(ScanForEnemyTarget());
    }

    public void GetDamaged( float damageValue, bool isCrit){
        //bool isDodge = (Random.Range(0,1f) < dodgeRate);
        if(!died){
            float actualDamage = damageValue * damageReceived;
            if(actualDamage > 0f) currentHealth -= actualDamage;
            HealthBar.transform.localScale = new Vector3(Mathf.Clamp(currentHealth/health, 0f, 1f), 1f, 1f);
            unitDisplay.SpawnDamageNumber(actualDamage, isCrit);
            if(currentHealth <= 0){
                died = true;
                Die();
            }
        }
    }

    public void GetHealed( float heal, bool isCrit){
        if(!died){
            float actualHeal = Mathf.Min(heal, health - currentHealth);
            currentHealth = Mathf.Min(currentHealth + heal, health);
            HealthBar.transform.localScale = new Vector3(Mathf.Clamp(currentHealth/health, 0f, 1f), 1f, 1f);
            unitDisplay.SpawnHealNumber(actualHeal, isCrit);
        }
    }

    private void Die(){
        if (!isPlayers) gameState.cash += 1;
        // notify spawner I died
        Spawner spawner = transform.parent.GetComponent<Spawner>();
        if(spawner != null) spawner.UnitDied();
        unitManager.CheckGameOver(isPlayers);
        GameObject.Destroy(gameObject, 1.1f);
        GetComponent<Animator>().SetBool("Dead", true);
        Destroy(this);
    }

    private IEnumerator PauseKiting(){
        float oldKitingEfficiency = kitingEfficiency;
        kitingEfficiency = 0;
        yield return new WaitForSeconds(0.5f);
        kitingEfficiency = oldKitingEfficiency;
    }

    private IEnumerator GetStunnedCor(float duration){
        float oldMoveSpeed = moveSpeed;
        moveSpeed = 0;
        nextAttackTime += duration;
        GameObject spiral = Instantiate(StunIndicator, new Vector3(transform.position.x, transform.position.y + 1f, 0f), Quaternion.identity);
        spiral.transform.parent = transform;
        yield return new WaitForSeconds(duration);
        GameObject.Destroy(spiral);
        moveSpeed = oldMoveSpeed;
    }

    public void GetStunned(float duration){
        StartCoroutine(GetStunnedCor(duration));
    }
}
