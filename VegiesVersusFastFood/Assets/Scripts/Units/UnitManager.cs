using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UnitManager : MonoBehaviour, IScheduledBehaviour
{
    public GameObject playerSpawnArea;
    public GameObject enemySpawnArea;
    public GameObject spawnersParent;

    public List<GameObject> startingPlayerSpawners;
    public List<GameObject> startingEnemySpawners;

    public List<GameObject> currentPlayerSpawners;
    private List<GameObject> currentEnemySpawners;

    private List<GameObject> currentPlayerSpawnerObjects;
    private List<GameObject> currentEnemySpawnerObjects;

    private List<GameObject> playerSpawnerPrefabs;
    private List<GameObject> enemySpawnerPrefabs;

    private UpgradeManager upgradeManager;

    public int Priority => 3;

    public void OnStartBehaviour()
    {
        currentPlayerSpawnerObjects = new List<GameObject>();
        currentEnemySpawnerObjects = new List<GameObject>();

        upgradeManager = UpgradeManager.instance;

        if (upgradeManager.ownedUnits == null || upgradeManager.ownedUnits.Count == 0)
        {
            upgradeManager.ownedUnits = new List<GameObject>(startingPlayerSpawners);
            currentPlayerSpawners = new List<GameObject>(startingPlayerSpawners);
            currentEnemySpawners = new List<GameObject>(startingEnemySpawners);
        }
        else
        {
            currentPlayerSpawners = new List<GameObject>(upgradeManager.ownedUnits);
            currentEnemySpawners = new List<GameObject>(DifficultyManger.Instance.newEnemySpawners.Count == 0 ? startingEnemySpawners : DifficultyManger.Instance.newEnemySpawners.ConvertAll(x => x.gameObject));
        }

        //foreach (GameObject spawner in currentPlayerSpawners) PlaceSpawnerInArea(spawner, playerSpawnArea, true);
        foreach (GameObject spawner in currentEnemySpawners) PlaceSpawnerInArea(spawner, enemySpawnArea, false);
    }

    private void PlaceSpawnerInArea(GameObject spawner, GameObject area, bool isPlayers)
    {
        Vector3 areaCenter = area.transform.position;
        float areaX = area.transform.localScale.x / 2;
        float areaY = area.transform.localScale.y / 2;

        float randomX = Random.Range(areaCenter.x - areaX, areaCenter.x + areaX);
        float randomY = Random.Range(areaCenter.y - areaY, areaCenter.y + areaY);

        GameObject spawnerObject = Instantiate(spawner, new Vector3(randomX, randomY, 0), Quaternion.identity, spawnersParent.transform);
        if (isPlayers) currentPlayerSpawnerObjects.Add(spawnerObject);
        else currentEnemySpawnerObjects.Add(spawnerObject);
    }

    public void StartSpawners()
    {
        foreach (GameObject spawner in currentPlayerSpawnerObjects) spawner.GetComponent<Spawner>().shouldStart = true;
        foreach (GameObject spawner in currentEnemySpawnerObjects) spawner.GetComponent<Spawner>().shouldStart = true;
    }

    public void AddPlayerSpawner(GameObject spawner)
    {
        currentPlayerSpawnerObjects.Add(spawner);
    }

    public void CheckGameOver(bool playerUnitDied)
    {
        bool allPlayerUnitsDead = true;
        bool allEnemyUnitsDead = true;
        foreach (GameObject spawner in currentPlayerSpawnerObjects)
        {
            if (spawner.GetComponent<Spawner>().GetSpawnsLeft() > -0.5f) allPlayerUnitsDead = false;
        }
        foreach (GameObject spawner in currentEnemySpawnerObjects)
        {
            if (spawner.GetComponent<Spawner>().GetSpawnsLeft() > -0.5f) allEnemyUnitsDead = false;
        }
        if (allPlayerUnitsDead && playerUnitDied)
        {
            GameState.instance.OnLost();
        }
        else if (allEnemyUnitsDead && !playerUnitDied)
        {
            GameState.instance.OnWon();
        }
    }
}
