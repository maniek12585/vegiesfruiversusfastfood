using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Spawner : MonoBehaviour
{
    [SerializeField] private float spawnerSpawnCost;
    public GameObject unitToSpawn;
    [SerializeField] private float SpawnDelay, timeWhenSpawns, startSpawnTime;
    public int SpawnLimit = 10, SpawnsLeft;
    private bool canSpawn = true, isUnitDead = false;
    [SerializeField] private TextMeshPro textMesh;
    [SerializeField] private Image timer;
    [SerializeField] public bool shouldStart = false;
    private bool timerStart = false;
    public float SpawnCost => spawnerSpawnCost;
    public float SpawnDelay_ => SpawnDelay;

    public void ChangeSpawnLimit(int newSpawnLimit) => SpawnLimit = newSpawnLimit;
    public void ChangeSpawnDelay(float newSpawnDelay) => SpawnDelay = newSpawnDelay;

    private void Awake() 
    {
        SpawnsLeft = SpawnLimit;
        textMesh.text = SpawnsLeft.ToString();
        GetComponentInChildren<SpriteRenderer>().sprite = unitToSpawn.GetComponent<UnitController>().unitTexture;
    }
    // Start is called before the first frame update
    void Start()
    {
        //SpawnsLeft--;
        //StartCoroutine(Spawning(2f));
        timer.enabled = false;
    }

    private void Update() {
        if (shouldStart)
        {
            SpawnsLeft--;
            StartCoroutine(Spawning(2f));
            shouldStart = false;
            timer.enabled = true;
            timerStart = true;
        }
        if (timerStart)
        {
            timer.fillAmount = 1 - ((Time.timeSinceLevelLoad - startSpawnTime) / (timeWhenSpawns - startSpawnTime));
        }
    }

    private IEnumerator Spawning(float delay){
        startSpawnTime = Time.timeSinceLevelLoad;
        timeWhenSpawns = startSpawnTime + delay;
        yield return new WaitForSeconds(delay);
        textMesh.text = SpawnsLeft.ToString();
        //canSpawn = false;
        GameObject newUnit = Instantiate(unitToSpawn, transform.position, Quaternion.identity);
        newUnit.GetComponent<UnitController>().Init();
        //isUnitDead = false;
        newUnit.transform.SetParent(transform);
        //canSpawn = true;
    }

    public void UnitDied(){
        SpawnsLeft--;
        if(SpawnsLeft > -0.1f){
            StartCoroutine(Spawning(SpawnDelay));
        }
    }

    public int GetSpawnsLeft()
    {
        return SpawnsLeft;
    }
}
