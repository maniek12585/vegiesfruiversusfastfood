using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceOnGrid : MonoBehaviour
{
    // Start is called before the first frame update
    private bool isDragged = false;
    private Vector3 mousePos;
    private GameObject hoveredTile;
    private Rigidbody2D rbody;
    private BoxCollider2D collider;
    //private MouseCollider mouseCollider;
    void Start()
    {
        //mouseCollider = GameObject.Find("MouseCollider").GetComponent<MouseCollider>();
        collider = gameObject.AddComponent<BoxCollider2D>();
        collider.isTrigger = true;
        rbody = gameObject.AddComponent<Rigidbody2D>();
        rbody.bodyType = RigidbodyType2D.Dynamic;
        rbody.gravityScale = 0;
        rbody.freezeRotation = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(isDragged)
        {
            mousePos = Input.mousePosition;
            var newPos = Camera.main.ScreenToWorldPoint(mousePos);
            rbody.MovePosition(newPos);
            //transform.position = new Vector3(newPos.x, newPos.y, 0);
        }
    }

    private void OnMouseDown()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if(isDragged && hoveredTile != null)
            {
                transform.position = hoveredTile.transform.position;
            } 
            isDragged = !isDragged;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Contains("Tile"))
        {
            hoveredTile = collision.gameObject;
        }
    }
}
