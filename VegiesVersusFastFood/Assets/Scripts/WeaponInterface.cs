using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class WeaponInterface : MonoBehaviour
{
    [SerializeField] protected float damage, critChance, critMulti, knockback, stunChance;
    [SerializeField] protected UnitController target;
    protected bool canAttack = false;
    protected float correctAngle = 0f;

    public void AttackWithWeapon(){
        canAttack = true;
    }
    
    public void SetWeaponTarget(UnitController newTarget){
        target = newTarget;
    }

    public void setDamageAndStuff(float newdamage, float newCritChance, float newCritMulti, float newKnockback, float newStunChance){
        damage = newdamage;
        critChance = newCritChance;
        critMulti = newCritMulti;
        knockback = newKnockback;
        stunChance = newStunChance;
    }

    public void FlippedLeft(bool left){
        if(left) correctAngle = 180f;
        else correctAngle = 0f;
    }

    protected bool IsCrit(){
        return (Random.Range(0,1f) < critChance);
    }

    protected bool IsStun(){
        return (Random.Range(0,1f) < stunChance);
    }
}
