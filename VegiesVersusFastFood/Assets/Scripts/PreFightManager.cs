using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreFightManager : MonoBehaviour
{

    [SerializeField] private Tile tile;
    private SpriteRenderer gridRenderer;
    private SpriteRenderer tileRenderer;
    private int tilesOnX, tilesOnY, counterX = 0, counterY = 0;
    private float tileWidth, tileHeight;

    void Start()
    {
        gridRenderer = GetComponent<SpriteRenderer>();
        tileRenderer = tile.GetComponent<SpriteRenderer>();
        tileWidth = tileRenderer.bounds.size.x;
        tileHeight = tileRenderer.bounds.size.y;
        tilesOnX = Mathf.RoundToInt(gridRenderer.bounds.size.x / tileWidth);
        tilesOnY = Mathf.RoundToInt(gridRenderer.bounds.size.y / tileHeight);
        GenerateGrid();
    }

    void GenerateGrid()
    {
        var drawingStart = transform.TransformPoint(new Vector3(
            gridRenderer.sprite.bounds.min.x,
            gridRenderer.sprite.bounds.min.y 
        ));
        drawingStart.x += tileWidth / 2;
        drawingStart.y += tileHeight / 2;
        counterX = 0;
        var newTilePositionX = 0f;
        var newTilePositionY = 0f;
        // tak, to jest wolne
        var gridArea = GameObject.Find("GridArea");
        for (var x = 0f; x < tilesOnX; x++)
        {
            counterY = 0;
            newTilePositionY = 0f;
            for(var y = 0f; y < tilesOnY; y++)
            {
                var tilePosition = new Vector3(drawingStart.x + newTilePositionX, drawingStart.y + newTilePositionY);
                var spawnedTile = Instantiate(tile, tilePosition, Quaternion.identity);
                spawnedTile.name = $"Tile-{counterX}{counterY}";
                var tileRenderer = spawnedTile.GetComponent<SpriteRenderer>();
                tileRenderer.sortingLayerName = "Background";
                tileRenderer.sortingOrder = 0;
                var isOffset = (counterX % 2 == 0 && counterY % 2 != 0) || (counterX % 2 != 0 && counterY % 2 == 0);
                spawnedTile.Init(isOffset);
                spawnedTile.transform.parent = gridArea.transform;
                counterY++;
                newTilePositionY += tileHeight;
            }
            counterX++;
            newTilePositionX += tileWidth;
        }
    }
}
