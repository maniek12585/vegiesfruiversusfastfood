using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum State
{
    InArena,
    InShop
}

public class GameState : MonoBehaviour, IScheduledBehaviour
{
    public static GameState instance;
    public int cash = 0;
    public State state = State.InArena;

    public int arenasWon = 0;

    public int Priority => 1;

    public void OnStartBehaviour()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (state == State.InArena)
            {
                state = State.InShop;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }
        }
        if(Input.GetKeyDown(KeyCode.LeftBracket)){
            cash += 10;
        }
        if(Input.GetKeyDown(KeyCode.RightBracket)){
            UnitController[] units = FindObjectsOfType<UnitController>();
            foreach(UnitController unit in units){
                if(!unit.isPlayers){
                    unit.GetDamaged(99, true);
                }
            }
        }
        if(Input.GetKeyDown(KeyCode.Escape)){
            Application.Quit();
        }
    }

    private IEnumerator BehaviourAfterBattle()
    {
        yield return new WaitForSeconds(2f);
        state = State.InShop;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void OnWon()
    {
        arenasWon++;
        DifficultyManger.Instance.MakeGameHarder(arenasWon);
        StartCoroutine(BehaviourAfterBattle());
    }

    public void OnLost()
    {
        StartCoroutine(BehaviourAfterBattle());
    }
}
