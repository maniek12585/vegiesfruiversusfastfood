using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeWeapon : WeaponInterface//, IScheduledBehaviour
{
    //private float damage;
    //private UnitController target;
   // private bool canAttack = false;
    private Rigidbody2D rb2d;

    //private float correctAngle = 0f;

    [SerializeField] private GameObject spriteContainer;

    private Vector2 vectorToTarget;

    public int Priority => 3;

    //public void setDamage(float newDamage){
    //    damage = newDamage;
    //}

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(target != null){
            vectorToTarget = target.transform.position - transform.parent.position;

            //rotate sword towards enemy
            var angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg + correctAngle;
            spriteContainer.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
        if(canAttack && target != null){
            Vector2 newPosition = Vector2.MoveTowards(transform.position, target.transform.position, Time.fixedDeltaTime * 15);
            rb2d.MovePosition(newPosition);
            float distanceToTarget = (transform.position - target.transform.position).magnitude;
            //Debug.Log(distanceToTarget);
            if(distanceToTarget < 0.5f)
            {
                // damage the unit
                canAttack = false;
                if(target.isDodge()){
                    target.GetDamaged(0f, false);
                } else{
                    if(IsStun()){
                        target.GetStunned(1.2f);
                    }
                    if (IsCrit())
                    {
                        target.GetDamaged(damage * critMulti, true);
                        applyKnockback(2f);
                    }
                    else{
                        target.GetDamaged(damage, false);
                        applyKnockback(1f);
                    }
                }
            }
        }
        else if(transform.localPosition.magnitude > Mathf.Epsilon){
            canAttack = false;
            Vector2 newPosition = Vector2.MoveTowards(transform.position, transform.parent.position, Time.fixedDeltaTime * 15);
            rb2d.MovePosition(newPosition);
        }
    }

    private void applyKnockback(float multi){
        Rigidbody2D targetBody = target.GetComponent<Rigidbody2D>();
        if (targetBody != null)
        {
            targetBody.AddForce(vectorToTarget.normalized * 100 * knockback * multi);
        }
    }

    //public void AttackWithWeapon(){
    //    canAttack = true;
    //}

    //public void SetWeaponTarget(UnitController targetUnit){
    //    target = targetUnit;
    //}

    //public void FlippedLeft(bool left){
    //   if(left) correctAngle = 180f;
    //    else correctAngle = 0f;
    //}
}
