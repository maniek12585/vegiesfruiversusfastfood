using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnersArea : MonoBehaviour, IScheduledBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private UnitManager manager;
    private SpriteRenderer areaRenderer;

    public int Priority => 4;

    void IScheduledBehaviour.OnStartBehaviour()
    {
        Init();
    }

    void Init()
    {
        var count = manager.startingPlayerSpawners.Count;
        var areaRenderer = transform.GetComponent<SpriteRenderer>();
        var drawingStart = transform.TransformPoint(new Vector3(
            areaRenderer.sprite.bounds.center.x,
            areaRenderer.sprite.bounds.center.y 
        ));
        var posY = 0f;
        drawingStart.y -= 1.61f/count;
        foreach(var t in manager.currentPlayerSpawners)
        {
            var position = new Vector3(drawingStart.x, drawingStart.y + posY);
            var spawnerObject = Instantiate(t, position, Quaternion.identity);

            // Add scripts
            spawnerObject.AddComponent<PlaceOnGrid>();

            manager.AddPlayerSpawner(spawnerObject);
            posY += 1.61f;
        }
    }
}
